//
//  ViewController.swift
//  WeatherApp
//
//  Created by JojoBandung on 8/19/19.
//  Copyright © 2019 com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private let weatherApi = "http://api.openweathermap.org/data/2.5/weather"
    
    private let weatherAppId = "711cf20414b612204ac4abc6c1228cd7"
    
    private let cityApi = "http://geodb-free-service.wirefreethought.com/v1/geo/cities?limit=10"
    
    let defaultCity = "Bandung"
    
    let defaultCityArray = ["London", "Tokyo", "Liverpool", "Bandung"]
    
    var searchResultArray : [CityDetail] = []
    
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var currentWeatherImage: UIImageView!
    
    @IBOutlet weak var londonWeatherImage: UIImageView!
    
    @IBOutlet weak var tokyoWeatherImage: UIImageView!
    
    @IBOutlet weak var liverpoolWeatherImage: UIImageView!
    
    @IBOutlet weak var temperature: UILabel!
    
    @IBOutlet weak var cityName: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchResultTableView: UITableView!
    
    @IBOutlet weak var searchResultTableViewTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchButton.layer.cornerRadius = 5
        
        searchBar.isHidden = true
        searchResultTableView.isHidden = true
        
        for cityName in defaultCityArray {
            if (cityName == defaultCity) {
                getWeather(city: cityName, query: true)
            } else {
                getWeather(city: cityName, query: false)
            }
        }
        
    }
    
    @IBAction func openSearchLocation(_ sender: UIButton) {
        scrollView.isHidden = true
        searchBarTopConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        searchBar.isHidden = false
    }
    
    func getWeatherForSearch(city: String) {
        let session = URLSession.shared
        
        guard let weatherURL = URL(string: "\(weatherApi)?q=\(city)&appid=\(weatherAppId)") else { return }
        
        let task = session.dataTask(with: weatherURL) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                print("Error:\n\(error)")
            }
            else {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        var city: String = ""
                        var temp: String = ""
                        var imgURL: UIImage? = nil
                        
                        if let main = json["main"] as? [String: Any] {
                            let minTemp = main["temp_min"] as? Double
                            let maxTemp = main["temp_max"] as? Double
                            
                            let minTempStr = (minTemp! - 273.15).rounded()
                            let maxTempStr = (maxTemp! - 273.15).rounded()
                            
                            temp = "\(minTempStr) - \(maxTempStr) ºC"
                            
                            city = (json["name"] as? String)!
                        }
                        if let weather = json["weather"] as? [Any] {
                            if let current = weather[0] as? [String: Any] {
                                let icon = current["icon"] as? String
                                let urlIcon = URL(string: "http://openweathermap.org/img/wn/\(icon!)@2x.png")
                                let iconData = try? Data(contentsOf: urlIcon!)
                                
                                imgURL = UIImage(data: iconData!)!
                            }
                        }
                        if (imgURL != nil) {
                            DispatchQueue.main.async {
                                self.searchResultArray.append(CityDetail(city: city, temp: temp, imgURL: imgURL!))
                                self.searchResultTableView.reloadData()
                            }
                        }

                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    func getWeather(city: String, query: Bool) {
        
        let session = URLSession.shared
        
        guard let weatherURL = URL(string: "\(weatherApi)?q=\(city)&appid=\(weatherAppId)") else { return }
        
        let task = session.dataTask(with: weatherURL) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                print("Error:\n\(error)")
            }
            else {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        if (query) {
                            if let main = json["main"] as? [String: Any] {
                                let minTemp = main["temp_min"] as? Double
                                let maxTemp = main["temp_max"] as? Double
                                
                                let minTempStr = (minTemp! - 273.15).rounded()
                                let maxTempStr = (maxTemp! - 273.15).rounded()
                                
                                DispatchQueue.main.async {
                                    self.temperature.text = "\(minTempStr) - \(maxTempStr) ºC"
                                }
                            }
                            DispatchQueue.main.async {
                                self.cityName.text = json["name"] as? String
                            }
                            
                        }
                        if let weather = json["weather"] as? [Any] {
                            if let current = weather[0] as? [String: Any] {
                                let icon = current["icon"] as? String
                                let urlIcon = URL(string: "http://openweathermap.org/img/wn/\(icon!)@2x.png")
                                let iconData = try? Data(contentsOf: urlIcon!)
                                DispatchQueue.main.async {
                                    if (query) {
                                        self.currentWeatherImage.image = UIImage(data: iconData!)
                                    } else {
                                        if (city.lowercased() == "liverpool") {
                                            self.liverpoolWeatherImage.image = UIImage(data: iconData!)
                                        } else if (city.lowercased() == "london") {
                                            self.londonWeatherImage.image = UIImage(data: iconData!)
                                        } else if (city.lowercased() == "tokyo") {
                                            self.tokyoWeatherImage.image = UIImage(data: iconData!)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    func getCityList(query: String) {
        let session = URLSession.shared
        let cityURL = URL(string: "\(cityApi)&namePrefix=\(query)")!
        
        let task = session.dataTask(with: cityURL) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                print("Error:\n\(error)")
            }
            else {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        if let list = json["data"] as? [Any] {
                            for x in 0..<list.count {
                                if let datum = list[x] as? [String: Any] {
                                    let cityName = datum["name"] as? String
                                    DispatchQueue.main.async {
                                        self.getWeatherForSearch(city: cityName!)
                                    }
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchResultTableViewTopConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        searchResultTableView.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBarTopConstraint.constant = 100
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        searchBar.isHidden = true
        searchBar.endEditing(true)
        searchResultTableViewTopConstraint.constant = 200
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        searchResultTableView.isHidden = true
        scrollView.isHidden = false
        searchBar.text = ""
        searchResultArray = []
        searchResultTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        getCityList(query: searchBar.text!)
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath)
        cell.textLabel?.text = searchResultArray[indexPath.row].city
        cell.imageView?.image = searchResultArray[indexPath.row].imgURL
        cell.detailTextLabel?.text = searchResultArray[indexPath.row].temp
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityName.text = searchResultArray[indexPath.row].city
        currentWeatherImage.image = searchResultArray[indexPath.row].imgURL
        temperature.text = searchResultArray[indexPath.row].temp
        
        self.searchBarCancelButtonClicked(searchBar)
    }
}

