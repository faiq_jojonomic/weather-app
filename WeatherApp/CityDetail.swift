//
//  CityDetail.swift
//  WeatherApp
//
//  Created by JojoBandung on 8/28/19.
//  Copyright © 2019 com. All rights reserved.
//

import Foundation
import UIKit

struct CityDetail {
    var city: String
    var temp: String
    var imgURL: UIImage
}
